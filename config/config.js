const config = require("./index");

module.exports = {
  development: {
    database: config.dbName,
    username: config.dbUser,
    password: config.dbPass,
    host: config.dbHost,
    port: config.dbPort,
    dialect: "postgres",
    logging: false,
  }
};
