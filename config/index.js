const dotenv = require("dotenv");
const path = require("path");

dotenv.config();

const env = process.env.NODE_ENV || "development";

const config = {
  env: env,
  isDevelopment: env === "development",

  port: process.env.PORT || 8000,
  rootPath: path.resolve(__dirname, ".."),

  dbHost: process.env.DB_HOST,
  dbPort: process.env.DB_PORT,
  dbUser: process.env.DB_USER,
  dbPass: process.env.DB_PASS,
  dbName: process.env.DB_NAME,
};

module.exports = config;
