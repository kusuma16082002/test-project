TAG ?= $(shell git rev-parse --abbrev-ref HEAD)
PWD=$(shell pwd)
include .env

build:
	docker build -t $(TAG) .

deploy:
	docker-compose build --pull && docker-compose up --force-recreate -d backend
	docker ps | grep 'docker'
test:
	@echo "Starting test..."
	@docker image rm -f bookshelf_backend || true
	@sleep 20
	@docker-compose up --force-recreate backend -d
	@docker-compose down -v || true
