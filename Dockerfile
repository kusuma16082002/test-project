# set base image
FROM node:14
# set working directory
WORKDIR /app
# copy files package*.json to /app
COPY package.json .
# install dependencies
RUN npm install
# copy source code to /app
COPY . .
# run app
CMD ["npm", "start"]
