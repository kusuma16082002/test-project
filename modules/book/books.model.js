("use strict");

module.exports = (sequelize, DataTypes) => {
  const Books = sequelize.define(
    "books",
    {
        book_name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: "Book name is not required"
                }
            }
        },
        price: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            validate: {
                notNull: {
                    msg: "Price is not required"
                }
            }
        },
        publisher: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: "Publisher is required"
                }
            }
        },
        publishedAt: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notNull: {
                    msg: "Published at is required"
                }
            }
        },
        year: {
            type: DataTypes.DATE,
            allowNull: false,
            validate: {
                notNull: {
                    msg: "Year is required"
                }
            }
        }
    });

    return Books;

};
