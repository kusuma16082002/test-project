const { models } = require("../../models");

const UserController = {
  getAll: async (req, res) => {
    try {
        const books = await models.books.findAll()
        .then((book) => {
        console.log(book);
        const respone = {
            count: book.length,
            books: book,
        }
        res.status(201).json(respone)
    })
    .catch((err) => res.status(404).json({ message: err}))
    } catch (error) {res.status(404).json({ message: error})}
  },

  getOne: async (req, res) => {
      try {
          const id = req.params.id;
          const books = await models.books.findOne({ where: { id: id }})
          .then((book) => {
            const respone = {
                count: book.length,
                books: book,
            }
            res.status(201).json(respone)
          })
          .catch((err) => res.status(404).json({ message: err}))
      } catch (error) {res.status(404).json({ message: error})}  
  },

  create: async (req, res) => {
      try {
          const { body }  = req;
          const book = await models.books.findByPk(body.id);

          if(book) {
            const respone = {
                message: "Existing Book"
            }
            res.status(201).json(respone)
          }else{
          
          const books = await models.books.create({ ...body })
          .then((book) => {
            const respone = {
                message: "New Book Created Successfully",
                count: book.length,
                books: book,
            }
            res.status(201).json(respone)
          })
          .catch((err) => res.status(404).json({ message: err}))
        }
      } catch (error) {res.status(404).json({ message: error})}
  }
}

module.exports = UserController;
