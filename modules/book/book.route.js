const express = require("express");
const {
  getAll,
  getOne,
  create
} = require("./book.controller");

const router = express.Router();

router.get("/books",
  getAll,
);

router.get("/books/:id", 
    getOne
);

router.post("/books", 
    create
);

module.exports = router;
