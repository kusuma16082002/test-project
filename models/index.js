"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const conf = require("../config");
const config = require(conf.rootPath + "/config/config.js")[conf.env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

const walkSync = (dir, files = []) => {
  fs.readdirSync(dir).forEach(file => {
    if (fs.statSync(path.join(dir, file)).isDirectory()) {
      files = walkSync(path.join(dir, file), files);
    } else {
      if (
        file.indexOf(".") > -1 &&
        file.slice(-8) === "model.js"
      ) {
        files.push(path.join(dir, file));
      }

    }
  });

  return files;
};

walkSync(conf.rootPath + "/modules")
  .forEach((file) => {
    const model = require(file)(
      sequelize,
      Sequelize.DataTypes
    );
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.models = sequelize.models;
db.Sequelize = Sequelize;

module.exports = db;
