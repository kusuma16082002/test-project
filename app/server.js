const express = require("express");
const http = require("http");
const cors = require("cors");
const config = require("../config");
const routes = require("../routes/index")
const { sequelize } = require("../models");
const { urlencoded, json } = require("body-parser");

const app = express();

app.use(cors());
app.use(urlencoded({ extended: false }));
app.use(json());

app.use("/api", routes);

const server = http.Server(app);
server.listen(config.port, async () => {
  try {
    await sequelize.authenticate();
    console.log("Database connection has been established");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
    process.exit(1);
  }

  console.info("LISTENING ON PORT:", config.port);
});

module.exports = server;
