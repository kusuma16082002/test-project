const { Router } = require("express");

const books = require("../modules/book/book.route");

const router = Router();
router.use(books);

module.exports = router;
